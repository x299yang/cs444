
import sys
from os import listdir
from os.path import isfile, join

from Scanning import scan
from Parsing import parse
import Weeding

def allFiles(testDir):
     return [testDir + f for f in listdir(testDir) if isfile(join(testDir, f)) and f.startswith('Je')]

def main():

    # All files in the test directory
    testDirectory = "./Tests/"
    testFiles = allFiles(testDirectory)
    # testFiles = ['./Tests/Je_1_IntRange_MinusTooBigInt.java']
    print("**********************************************************")

    for f in testFiles:
        # print(f)

        # Scanning
        content = open(f, "r").read()
        (tokens, errorString) = scan(content)
        # Error in Scanning
        if tokens is None:
            # print("ERROR in Scanning: " + errorString)
            # print("**********************************************************")
            continue

        # s = "All Tokens: "
        # for token in tokens:
        #     if (token.name and token.lex):
        #         s += '(' + token.name + ',' + token.lex + '), '
        # print(s)

        # No weeds if everything is good (weeds = None)
        weeds = Weeding.fileNameCheck(tokens, f)
        if weeds:
            # print(weeds)
            continue



        # Parsing
        # print("Parsing starts")

        try:
            (steps, errorString) = parse(tokens)
        except:
            print("Exception in Parsing")

        # Error in Parsing
        if steps is None:
            # print("ERROR in Parsing: ", errorString)
            # print("**********************************************************")
            continue

        print(f)
        print("Succeeded")
        # print("All Steps:")
        # print(steps)

        print("**********************************************************")

main()