import string
import os

##################### Weeding after scanning ##########################################
def weedTokens(tokens, file):
    result = fileNameCheck(tokens, file)
    result += extendCheck(tokens)
    # result += intRangeCheck(tokens)
    return result


def fileNameCheck(tokens, f):
    fileNameParts = os.path.basename(f).split('.java')
    if len(fileNameParts) < 2:
        fileNameParts = os.path.basename(f).split('.joos')
    fileName = fileNameParts[0]

    check = False
    for t in tokens:
        if check:
            if (t.lex != fileName):
                return ("ERROR: Class or Interface name should be the same as file name. " + t.lex + "  " +  fileName + "\n")
            return ""
        if t.name == 'INTERFACE' or t.name == 'CLASS':
            check = True
    return ""

def extendCheck(tokens):
    check = False
    for t in tokens:
        if check:
            if (not t.lex.startswith('java.' )):
                return ("ERROR: Can not extend/implement class type " +  t.lex + ".  \n")
            return ""
        if t.name == 'EXTENDS' or t.name == 'IMPLEMENTS':
            check = True
    return ""

# def intRangeCheck(tokens):
#     for index, t in enumerate(tokens):
#         if t.name == 'NUM' and index > 0 and t[index-1].name == 'SUB' and int(t.lex) > 2147483648:
#             return ("ERROR: integer too small")
#         if t.name == 'NUM' and int(t.lex) > 2147483647 and (index is 0 or t[index-1].name is not 'SUB'):
#             return ("ERROR: integer too large")
#     return ""


######################## Weeding after parsing ################################


# node: Node[]
# def oneConstructor(node, insideClass):
#     success = False
#     if not insideClass:
#         if node.name == 'classDcl':
#             for c in node.children:
#                 success = oneConstructor(c, True)
#                 if success:
#                     return True
#     elif insideClass:
#         if node.name == 'constructorDcl':
#             return True


# tree: Node[]
# def weed(tree):
#     # Every class must contain at least one explicit constructor
#     if oneConstructor(tree, False) == False:
#         print('a class does not have an explicit constructor')
