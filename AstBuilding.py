from CompNode import CompNode
from pprint import pprint
from Environment import GlobalEnv
from TypeNodes import getSupers
from CodeGenUtils import p, genProcedure, used_labels, local_labels, pLabel, genericHelperFunctions, globalImport

# tree: (filename, parseTree)
def astBuild(trees):
    ASTs = []
    for (n, t) in trees:
        ASTs.append((n, CompNode(t)))
    return ASTs

def buildEnvAndLink(ASTs):
    # build env
    globalEnv = GlobalEnv()
    for t in ASTs:
        globalEnv.addtoEnv(t[1])

    # print ("\n\n###################### global Env ####################")
    # pprint(vars(globalEnv))
    globalEnv.weedGlobalEnv()

    for t in ASTs:
        try:
            t[1].recurseBuildEnv(globalEnv)
        except Exception as e: # to handle double local variable declaration
            raise e
        # print('\n\n\n', t[0])
        # print("###################### Comp Unit Env ####################")
        # t[1].recurseAction("printEnv")
        # if t[0] == "./Tests/A5/J1_02_Array_Length.java":
        #     t[1].printTree()


    # type Linking
    for t in ASTs:
        t[1].recurseAction("linkType")

    # hierarchy checking
    for t in ASTs:
        t[1].recurseAction("checkHierarchy")

#######################################################

def disamiguateAndTypeChecking(ASTs):

    # disambiguate namespace: figuring out the meaning of the shortest prefix of names
    for t in ASTs:
        t[1].disambigName()

    # type checking
    for t in ASTs:
        t[1].checkType()

#######################################################

def reachabilityChecking(ASTs):
    for t in ASTs:
        t[1].reachCheck()

####################################################
# Returns a dictionary (type e.g. "boolean") -> str (the assembly code generated)
def arrayClassMemory(types):
    # All method labels from java.lang.Object
    methods = ["M_Object_Object_", "M_Object_equals_Object", "M_Object_toString_", "M_Object_hashCode_", "M_Object_clone_", "M_Object_getClass_"]
    primTypes = ['boolean', 'byte', 'char', 'int', 'short'] # All possible types
    typeDict = {}
    # assignable = dict({"java.lang.Object" : 0,
    # "java.lang.Cloneable" : 4,
    # "java.io.Serializable" : 8
    # })

    for tt in types:
        # Class memory layout
        # Label: "A_type"
        # Label for SIT pointer: "I_SIT_spot_typeName_array"
        # Label for method pointers: "V_typeName_methodName_param_array"
        if tt.__class__.__name__ == "ClassNode":
            t = tt.name
        else:
            t = tt

        code = "section .data\n"
        code += "; Start of class memory layout\n"
        code += pLabel(name=t, type="array") + \
                pLabel("SIT_spot_"+ t + "_array", "inter") + \
                p(instruction="dd", arg1=64)

        # point to subtype table
        code += pLabel("subT_spot_" + t, "inter")
        code += p("dd", "42")

        for m in methods:
            code += pLabel(name=t+"_"+m+"_array", type="vtable") + \
                    p(instruction="dd", arg1=64)

        code += "; End of class memory layout\n"

        # Creating a function to initialize class memory layout
        # Function label: H_typeName_arrayMemoryInitialize
        code += "section .text\n" + \
                "; Function to initialize class memory layout\n" + \
                pLabel(name=t+"_arrayMemoryInitialize", type="helper") + \
                p(instruction="mov", arg1="eax", arg2="dword I_SIT_spot_"+t+ "_array") + \
                p(instruction="extern", arg1="I_SIT_Object") + \
                p(instruction="mov", arg1="[eax]", arg2="dword I_SIT_Object", comment="Points to the SIT column of java.lang.Object")

        for m in methods:
            code += p(instruction="extern", arg1=m) + \
                    p(instruction="mov", arg1="eax", arg2="dword V_"+t+"_"+m+"_array") + \
                    p(instruction="mov", arg1="[eax]", arg2="dword "+m, comment="points to method implementation")

        if tt.__class__.__name__ == "ClassNode":
            code += p("extern", "A_subT_" + tt.name)
            code += p("mov", "eax", "I_subT_spot_" + t)
            code += p("mov", "[eax]", "dword A_subT_" + tt.name)

        code += p(instruction="ret", arg1="")
        code += "; End of function to initialize class memory layout\n"
        if t in primTypes:
            typeDict[('P', t)] = code # primitive types (for file name)
        else:
            typeDict[('T', t)] = code # non primitive types

    return typeDict


# Preparation before code Gen:
# 1. Calculating the size of the object from each class
#    (also simultaneosly creating fieldOffset because fieldOffset table is required to generate size)
# 2. SIT building
# 3. Creating class memory layout for every possible array types
def codeGenPrep(ASTs):
    interM = []
    types = ['boolean', 'byte', 'char', 'int', 'short'] # All possible types
    classNodes = []
    j = 0

    for t in ASTs:
        classInterNode = t[1].typeDcl
        if classInterNode.__class__.__name__ == "ClassNode":
            types.append(classInterNode)
            classNodes.append(classInterNode)
        else: # interfaceNode, get their methods to prep for SIT
            interM += classInterNode.methods

        classInterNode.subTypeOffset = j * 4 # assign each type a position in the subtype testing table
        j += 1

    # store SIT and subtype table size
    for c in classNodes:
        c.SITsize = len(interM)
        c.subTypeSize = j  # no primitive types in subtype table

    # prep SIT
    for i in range(len(interM)):
        interM[i].methodOffset = i * 4
        interM[i].isInterM = True

    # init data section
    for c in classNodes:
        c.populateSizeAndFieldOffset()
        c.populateMethodOffset()

    return arrayClassMemory(types)


def codeGen(ASTs, output="output"):
    arrayClassMemory = codeGenPrep(ASTs)
    for t in ASTs:
        t[1].codeGen()
    # Outputting the generated code into className.s
    # Note: Interfaces do not generate any code
    flagGlobalHelper = False # a flag for whether or not the global helper functions are already outputted to a file
    for t in ASTs:
        classInterNode = t[1].typeDcl
        if classInterNode and classInterNode.__class__.__name__ == "ClassNode":
            fileName = output + "/" + classInterNode.name + ".s"
            f = open(fileName, "w")
            f.write("section .data\n")
            f.write(classInterNode.data)
            f.write("section .text\n")
            if not flagGlobalHelper:
                f.write(globalImport(True, classInterNode.name))
                f.write(genericHelperFunctions())
                flagGlobalHelper = True
            else:
                f.write(globalImport(False, classInterNode.name))
            f.write(classInterNode.code)
            f.close()

    # ASTs[0][1].codeGen()
    startGen(ASTs, output, arrayClassMemory) # pass in the first AST that contains the test method

    # reset lables for running multiple testCases
    used_labels = set()
    local_labels = 0

def startGen(ASTs, output, arrayClassMemory):

    # Test method lable
    ast = ASTs[0][1]
    className = ast.typeDcl.name
    method_label = "M_" + className + "_test_"

    callInit = "; Start of calling each class's classAndStaticFieldInit and globalImport\n"
    for t in ASTs:
        classInterNode = t[1].typeDcl
        if classInterNode and classInterNode.__class__.__name__ == "ClassNode":
            label1 = "H_" + classInterNode.name + "_" + "classAndStaticFieldInit"
            label2 = "H_" + classInterNode.name + "_" + "globalImport"
            callInit += p(instruction="extern", arg1=label1) + \
                        p(instruction="call", arg1=label1) + \
                        p(instruction="extern", arg1=label2) + \
                        p(instruction="call", arg1=label2)
    callInit += "; End of calling each class's classAndStaticFieldInit \n"

    callArrayInit = "; Start of calling each array type's arrayMemoryInitialize\n"
    for t, code in arrayClassMemory.items():
        label = "H_" + t[1] + "_arrayMemoryInitialize"
        callArrayInit += p(instruction="extern", arg1=label) + \
                         p(instruction="call", arg1=label)
        # Creating .s files for each array type to store the class memory layout
        fileName = output + "/" + t[0] + '_' + t[1] + "_arrayClass" + ".s"
        f = open(fileName, "w")
        f.write(code)
        f.close()
    callArrayInit += "; End of calling each array type's arrayMemoryInitialize\n"


    f = open( output + "/RunTest.s","w")

    result = "      global _start\n_start:\n" \
    + callInit \
    + callArrayInit \
    + p("extern", method_label, None, None) \
    + p("call", method_label, None, None) \
    + p("mov", "ebx", "eax", " move eax to exit code") \
    + p("mov", "eax", "1", " exit command to kernel") \
    + p("int", "0x80", None, " interrupt 80 hex, call kernel")
    f.write(result)
    f.close()

    # generate dummy test file #### TODO: remove
    # f1 = open("output/"+ className + ".s","w")
    # f1.write("      global "+ method_label + "\n" + method_label+ ":\n")
    # f1.write(genProcedure(p("mov", "eax", "42", " exit command to kernel"), "Dummy test procedure"))
    # f1.close()
