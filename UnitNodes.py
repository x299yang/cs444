from AST import ASTNode, getParseTreeNodes
from Environment import Env
from TheTypeNode import TypeNode, TypeStruct
from CodeGenUtils import p, getLStringLabel, importHelper

#   LiteralNode
#   ParamNode

# maybe add a nameNode?
#################################################################################
# literals
class LiteralNode(ASTNode):
    toLiType = dict({
        'LITERALBOOL': 'boolean',
        'LITERALCHAR': 'char',
        'LITERALSTRING': 'java.lang.String',
        'NULL': 'null',
        'NUM': 'int',
        'ZERO': 'int'
    })
    # always list all fields in the init method to show the class structure
    def __init__(self, parseTree, typeName):
        self.parseTree = parseTree
        self.name = LiteralNode.toLiType.get(parseTree.children[0].name)  # type of the literal
        self.value = parseTree.children[0].lex # the value
        self.myType = TypeStruct(self.name, None)
        self.env = None
        self.children = []
        self.typeName = typeName

        if self.name == 'int':
            self.value = int(self.value)
        if self.name == 'LITERALBOOL':
            if self.value == 'false':
                self.value = False
            else:
                self.value = True
        if self.value == "'\\n'":
            self.value = ord('\n')
        elif self.name == 'char' and len(self.value) == 3:
            self.value = ord(self.value[1])
        elif self.name == 'char':
            self.value = ord(self.value)


    def linkType(self):
        if self.name == "java.lang.String":
            node = self.env.getNode(self.name, "type")
            self.myType = TypeStruct(self.name, node)

    def codeGen(self):
        if hasattr(self, "code") and self.code != "":
            return
        self.code = ""

        # boolean
        if self.name == 'boolean':
            if self.getConstant():
                self.code += p("mov", "eax", "1", " set to literal true")
                return
            else:
                self.code += p("mov", "eax", "0", " set to literal false")
                return

        # char
        if self.name == 'char':
            # store literal char as ASCII
            self.code += p("mov", "eax", str(self.getConstant()), " set to literal char")
            return

        # string
        if self.name == 'java.lang.String':
            self.code += ";Start of String Literal Creation for class " + self.typeName + "\n"
            # generate array of characters
            # then call new String(array)

            # remove quotation marks
            value = self.value[1:-1]

            # FIRST: create char array for this string
            # 1. Allocating space for the char array in heap
            # Note: uses ecx to temporarily store the array length
            self.code += ";Start of char array creation\n" + \
                p(instruction="mov", arg1="eax", arg2=str(len(value)), comment="array length") + \
                p(instruction="push", arg1="ecx", comment="saving ecx's value") + \
                p(instruction="mov", arg1="ecx", arg2="eax", comment="ecx now stroes the array's length") + \
                p(instruction="add", arg1="eax", arg2=2, comment="number of items to allocate on heap") + \
                p(instruction="imul", arg1="eax", arg2=4, comment="number of bytes to allocate on heap") + \
                p(instruction="call", arg1="__malloc", comment="allocating memory for array on heap")

            # 2. Pointing first item to vtable
            aLabel = "A_char"
            self.code += p(instruction="extern", arg1=aLabel) + \
                p(instruction="mov", arg1="[eax]", arg2="dword "+aLabel, comment="first item is vtable pointer")

            # 3. Storing length in the second slot
            self.code += p(instruction="mov", arg1="[eax+4]", arg2="ecx", comment="storing array length in second slot")

            # 4. populate array with this string literal's chars
            self.code += ";Start of populating char array\n"
            # loop through string
            for i in range(len(value)):
                self.code += p(instruction="mov", arg1="[eax+"+str(8+i*4)+"]", arg2="dword " + str(ord(value[i])), comment="a["+str(i)+"]="+str(value[i]))
            self.code += ";End of populating char array\n"

            # 5. Restoring ecx
            self.code += p(instruction="pop", arg1="ecx", comment="restoring register ecx")
            self.code += ";End of char array creation\n"

            # SECOND: create new string object with the char array

            # push array pointer
            self.code += p(instruction="push", arg1="eax", comment="save pointer to char array")

            # 1. alloc enough space for this object
            classDef = self.env.getNode('java.lang.String', 'type')
            fieldOffset = classDef.fieldOffset
            numFields = len(fieldOffset)
            numBytes = (numFields + 1) * 4
            self.code += "; Creating an object for class " + self.typeName + "\n"
            self.code += p(instruction="mov", arg1="eax", arg2=numBytes, comment="allocating memory for object") + \
                         p(instruction="call", arg1="__malloc")

            # 2. Pointing first item to vtable
            self.code += importHelper(classDef.name, self.typeName, "C_"+classDef.name)
            self.code += p(instruction="mov", arg1="[eax]", arg2="dword C_" + classDef.name, comment="first item is vtable pointer")

            # restore pointer to our char array
            self.code += p("pop", "ebx", comment="restore pointer to our char array")

            # 3. Calling constructor
            self.code += "; Calling constructor for String\n"
            # Evaluate arguments and pushing parameters
            self.code += p("push", "ebx", comment="add our char array as arg")
            self.code += p(instruction="push", arg1="eax", comment="pushing object as last argument")


            # 4. call String::String(char[] chars)
            label = "M_String_String_charArray"
            self.code += importHelper(classDef.name, self.typeName, label)
            self.code += p(instruction="call", arg1=label, comment="Calling constructor")

            # 4. Popping parameters and pointer to object
            self.code += p(instruction="pop", arg1="eax", comment="eax now contains pointer to newly created object")
            self.code += p("add", "esp", "4", "Popping parameters")
            self.code += ";End of object creation\n"
            self.code += ";End of String Literal Creation for class " + self.typeName + "\n"
            return

        # null
        if self.name == 'null':
            self.code += p("mov", "eax", "0", " set to literal null")
            return

        # int
        if self.name in ['int', 'short']:
            self.code += p("mov", "eax", str(self.getConstant()), " set to literal num")
            return

    def getConstant(self):
        if self.name == 'int':
            return int(self.value)
        elif self.name == 'boolean':
            if self.value == 'true':
                return True
            return False
        return self.value

##################################################################################
# param type ID
class ParamNode(ASTNode):
    # always list all fields in the init method to show the class structure
    def __init__(self, parseTree, typeName):
        self.parseTree = parseTree
        self.paramType = TypeNode(parseTree.children[0], typeName)
        self.name = parseTree.children[1].lex
        self.env = None
        self.children = [self.paramType]
        self.typeName = typeName
        self.offset = 0 # offset on the stack

    def checkType(self):
        self.myType = self.paramType.myType
