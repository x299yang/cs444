from AST import ASTNode, getParseTreeNodes
from Environment import Env
from UnitNodes import LiteralNode
import MemberNodes
from TheTypeNode import TypeNode, TypeStruct
from NameNode import NameNode, checkProtected
from CodeGenUtils import p, pLabel, genMethodInvoke, importHelper, getCFlowLabel
from codeGenNodes import genNameNode

# file containing smaller (lower level nodes) in the AST
# nodes in this file:
#   ArgsNode
#   ArrayAccessNode
#   ArrayCreateNode
#   AssignNode
#   CastNode
#   ClassCreateNode
#   ExprNode
#   FieldAccessNode
#   MethodInvNode

# TODO: go over nodes in this file to see if need to overright buildEnv

###########################################################
# factory methods
###########################################################

# parses the expr node in the parse tree to be either ID or CastNode or ExprNode
def makeNodeFromExpr(parseTree, typeName):
    c = parseTree
    while (42):
        if c.name == 'primaryAndArray':
            return makeNodeFromAllPrimary(c, typeName)
        elif c.name == 'ID':
            return NameNode(c, False, typeName) # TODO is this always False??
        elif c.name == 'COMPID':
            return FieldAccessNode(c, typeName, True)
        elif c.name == 'assignment':
            return AssignNode(c, typeName)
        elif c.name == 'refType':
            return TypeNode(c, typeName)
        elif len(c.children) == 1:
            c = c.children[0]
        elif c.name == 'castExpr':
            return CastNode(c, typeName)
        else:
            return ExprNode(c, typeName)

# parses the primaryAndArray/primary/primaryNoArrayAccess node in the parse tree and return corresponding AST nodes
def makeNodeFromAllPrimary(parseTree, typeName):
    if parseTree.name == 'primaryAndArray':
        if parseTree.children[0].name == 'arrayCreationExpr':
            parseTree = parseTree.children[0]
            return ArrayCreateNode(parseTree, typeName)
        elif parseTree.children[0].name == 'primary':
            if parseTree.children[0].children[0].name == 'arrayAccess':
                return ArrayAccessNode(parseTree.children[0].children[0], typeName)
            parseTree = parseTree.children[0].children[0]

    if parseTree.name == 'primary':
        if parseTree.children[0].name == 'arrayAccess':
            return ArrayAccessNode(parseTree.children[0], typeName)
        parseTree = parseTree.children[0]

    node = parseTree.children[0]
    if node.name == 'literal':
        return LiteralNode(node, typeName)
    elif node.name == 'LPAREN':  # primaryNoArrayAccess LPAREN expr RPAREN
        return makeNodeFromExpr(parseTree.children[1], typeName)
    elif node.name == 'classInstanceCreate':
        return ClassCreateNode(node.children[0], typeName)
    elif node.name == 'methodInvoc':
        return MethodInvNode(node, typeName)
    elif node.name == 'fieldAccess':
        return FieldAccessNode(node, typeName)
    else:
        raise Exception('ERROR: something wrong at primaryNoArrayAccess')

###########################################################
# helper methods
###########################################################
def helperDisambigName(node):
    if node and node.__class__.__name__ == "NameNode":
        try:
            node.disambigName()
        except Exception as e:
            raise e

###########################################################

########################### Node Definitions #####################################

###################################################################################
# args exprs, exprs expr COMMA exprs
class ArgsNode(ASTNode):
    # always list all fields in the init method to show the class structure
    def __init__(self, parseTree, typeName, exprs = []):
        self.parseTree = parseTree
        self.exprs = []  # a list of expressions
        self.env = None
        self.children = []
        self.typeName = typeName # the type (class/interface) this node belongs under

        if exprs == []:
            exprs = getParseTreeNodes(['expr'], parseTree)
            for e in exprs:
                self.exprs.append(makeNodeFromExpr(e, typeName))
        else:
            # manually induce exprs
            # for converting string concatenation to a method call
            # see ExprPrimaryNodes:ExprNode on string concat
            self.exprs = exprs

        self.children.extend(self.exprs)

    def disambigName(self):
        for expr in self.exprs:
            expr.disambigName()
            # helperDisambigName(expr)

    def codeGen(self):
        if hasattr(self, "code"):
            return
        self.code = ""
        # Generating code for each of the arguments
        for arg in self.exprs:
            if arg and hasattr(arg, "codeGen"):
                # children hasn't generated code yet
                # Note: this check is redundant if we're certain that every override of this method has the initial check
                if not hasattr(arg, "code"):
                    arg.codeGen()
                self.code += arg.code
                self.code += p(instruction="push", arg1="eax", comment="pushing result of evaluation of argument")


###################################################################################
# Array Access
class ArrayAccessNode(ASTNode):
    # always list all fields in the init method to show the class structure
    def __init__(self, parseTree, typeName):
        self.parseTree = parseTree
        self.array = ''  # either a variableName, a field, or array access
        self.index = '' # expr
        self.env = None
        self.children = []
        self.typeName = typeName # the type (class/interface) this node belongs under

        # input parse tree is either: arrayAccess name LSQRBRACK expr RSQRBRACK
        # arrayAccess ID LSQRBRACK expr RSQRBRACK
        # arrayAccess primaryNoArrayAccess LSQRBRACK expr RSQRBRACK
        if (parseTree.children[0].name == 'primaryNoArrayAccess'):
            self.array = makeNodeFromAllPrimary(parseTree.children[0], typeName)
        else:
            self.array = NameNode(parseTree.children[0], False, typeName)
        self.index = makeNodeFromExpr(parseTree.children[2], typeName)

        self.children.append(self.array)
        self.children.append(self.index)

    def disambigName(self):
        self.array.disambigName()
        self.index.disambigName()

    def checkType(self):
        self.array.disambigName() # hacky fix, not sure why disambigName wasn't called before
        self.array.checkType()
        self.index.checkType()
        if not self.array.myType.isArray:
            raise Exception("ERROR: Cannot perform array access on non-array {}".format(self.array.name))
        if not self.index.myType.isNum():
            raise Exception("ERROR: Array index must be a number.")
        self.myType = TypeStruct(self.array.myType.name, self.array.myType.typePointer)

    def getIfFalse(self, label):
        self.codeGen()
        return self.code + p("cmp", "eax", "[G__Zero]") + p("je", label)

    def addr(self):
        result = "; Start of calculating address for array access\n" + \
                 p(instruction="push", arg1="ecx", comment="saving register ecx")
        # 1. Generating code for a
        if not hasattr(self.array, "code"):
            self.array.codeGen()
        result += "; Evaluating pointer to array\n" + \
                  self.array.code + \
                  p(instruction="push", arg1="eax", comment="push arrray pointer")

        # 2. Evaluating i
        if not hasattr(self.index, "code"):
            self.index.codeGen()
        result += "; Evaluating index i\n"+ \
                   self.index.code + \
                   p(instruction="mov", arg1="ecx", arg2="eax", comment="ecx stores index i")

        # 3. Runtime checks
        result += p(instruction="pop", arg1="eax", comment="eax is pointer to array") + \
                  p(instruction="call", arg1="H__Null_Check") + \
                  p(instruction="call", arg1="H__Bounds_Check")

        # 4. Make eax store the address to the item we're accessing
        result += p(instruction="add", arg1="eax", arg2=8, comment="eax points at a[0]") + \
                  p(instruction="imul", arg1="ecx", arg2=4, comment="ecx contains the number of bytes to shift according to i") + \
                  p(instruction="add", arg1="eax", arg2="ecx", comment="eax now points at a[i]") + \
                  p(instruction="pop", arg1="ecx", comment="restoring ecx")

        result += "; End of calculating address for array access\n"

        return result

    def codeGen(self):
        if hasattr(self, "code"):
            return
        self.code = "; Array access\n" + \
                    self.addr() + \
                    p(instruction="mov", arg1="eax", arg2="[eax]", comment="eax now contains a[i]") +\
                    "; End of array access\n"



###################################################################################
# arrayCreationExpr
# arrayCreationExpr NEW primitiveType LSQRBRACK expr  RSQRBRACK
# arrayCreationExpr NEW name LSQRBRACK expr RSQRBRACK
class ArrayCreateNode(ASTNode):
    # always list all fields in the init method to show the class structure
    def __init__(self, parseTree, typeName):
        self.parseTree = parseTree
        self.arrayType = ''
        self.arraySize = 0 # or Expr
        self.env = None
        self.children = []
        self.typeName = typeName # the type (class/interface) this node belongs under

        # input is arrayCreationExpr NEW type LSQRBRACK expr RSQRBRACK
        self.arrayType = TypeNode(parseTree.children[1], typeName)

        expr = getParseTreeNodes(['expr'], parseTree)
        if len(expr) > 0:
            self.arraySize = makeNodeFromExpr(expr[0], typeName)

        self.children.append(self.arrayType)
        self.children.append(self.arraySize)

    def disambigName(self):
        self.arraySize.disambigName()
        # helperDisambigName(self.arraySize)

    def checkType(self):
        if self.arraySize != 0:
            self.arraySize.checkType()
        if not self.arraySize.myType.isNum():
            raise Exception("ERROR: Array index must be a number.")
        self.myType = TypeStruct(self.arrayType.myType.name, self.arrayType.myType.typePointer)
        self.myType.isArray = True

    def codeGen(self):
        if hasattr(self, "code"):
            return
        self.code = "; Creating an array\n"

        # 1. Allocating space for the array in heap
        # Note: uses ecx to temporarily store the array length
        if not hasattr(self.arraySize, "code"):
            self.arraySize.codeGen()
        self.code += "; Evaluating array length\n" + \
                     self.arraySize.code + \
                     " ; End of evaluating array length\n" + \
                     p(instruction="push", arg1="ecx", comment="saving ecx's value") + \
                     p(instruction="mov", arg1="ecx", arg2="eax", comment="ecx now stroes the array's length") + \
                     p(instruction="add", arg1="eax", arg2=2, comment="number of items to allocate on heap") + \
                     p(instruction="imul", arg1="eax", arg2=4, comment="number of bytes to allocate on heap") + \
                     p(instruction="call", arg1="__malloc", comment="allocating memory for array on heap")

        # 2. Pointing first item to vtable
        aLabel = "A_"
        if self.arrayType.myType.isPrimitive:
            aLabel += self.arrayType.myType.name
        else:
            aLabel += self.arrayType.myType.typePointer.name

        self.code += p(instruction="extern", arg1=aLabel) + \
                     p(instruction="mov", arg1="[eax]", arg2="dword "+aLabel, comment="first item is vtable pointer")

        # 3. Storing length in the second slot
        self.code += p(instruction="mov", arg1="[eax+4]", arg2="ecx", comment="storing array length in second slot")

        # 4. Restoring ecx
        self.code += p(instruction="pop", arg1="ecx", comment="restoring register ecx")
        self.code += ";End of array creation\n"

###################################################################################
# assignment leftHandSide ASSIGN expr
class AssignNode(ASTNode):
    # always list all fields in the init method to show the class structure
    def __init__(self, parseTree, typeName):
        self.parseTree = parseTree
        self.left = None
        self.right = makeNodeFromExpr(parseTree.children[2], typeName)
        self.env = None
        self.children = []
        self.typeName = typeName # the type (class/interface) this node belongs under

        if parseTree.children[0].children[0].name == 'fieldAccess':
            self.left = FieldAccessNode(parseTree.children[0].children[0], typeName)
        elif parseTree.children[0].children[0].name == 'arrayAccess':
            self.left = ArrayAccessNode(parseTree.children[0].children[0], typeName)
        else:
            self.left = NameNode(parseTree.children[0].children[0], False, typeName)

        self.children.append(self.right)
        self.children.append(self.left)

    def disambigName(self):
        self.left.disambigName()
        self.right.disambigName()
        # helperDisambigName(self.right)
        # helperDisambigName(self.left)

    def checkType(self):
        self.left.checkType()
        self.right.checkType()

        if self.left.myType.assignable(self.right.myType):
            self.myType = self.left.myType
            return

        raise Exception("ERROR: assignment operation failed. Cannot assign type {0} to type {1} at class {2}".format(self.left.myType.name, self.right.myType.name, self.typeName))

    def reachCheck(self, inMaybe):
        if not inMaybe:
            raise Exception("ERROR: not reaching a assignment statement")
        self.outMaybe = inMaybe

    def codeGen(self):
        self.code = ""

        # array assign check
        if self.left.__class__.__name__ == "ArrayAccessNode" and (not self.left.myType.isPrimitive):
            self.code += "; array assign subtype check\n"
            self.left.array.codeGen()
            self.code += self.left.array.code
            self.code += p("push", "eax")

            offset = self.right.myType.typePointer.subTypeOffset
            self.code += p("mov", "ebx", "[eax]", "start subtype test") # access class tag of left object
            self.code += p("mov", "ebx", "[ebx + 4]") # access subtype testing column
            self.code += p("mov", "ebx", "[ebx + " + str(offset) + "]") # ebx has isSubtype

            # exception if not subtype, else do nothing (object is already in eax)
            self.code += p("cmp", "[G__Zero]", "ebx")
            self.code += p("je", "H__Throw_Exception")

        self.code +=("; Evaluate left of assignment\n")
        self.code += self.left.addr()
        self.code += p("push", "eax")

        self.code +=("; Evaluate right of assignment\n")
        self.right.codeGen()
        self.code += self.right.code

        self.code += p("pop", "ebx")
        self.code += p("mov", "[ebx]", "eax")
        self.code += ("; End of assignment\n")


##################################################################################
# cast: castExpr LPAREN castType RPAREN unaryNotPlusMinus
class CastNode(ASTNode):
    # always list all fields in the init method to show the class structure
    def __init__(self, parseTree, typeName, left = None, right = None):
        self.parseTree = parseTree
        self.env = None
        self.children = []
        self.typeName = typeName # the type (class/interface) this node belongs under

        if left is None or right is None:
            self.left = parseTree.children[1]  # cast: (left)right
            self.right = makeNodeFromExpr(parseTree.children[3], typeName) # expr

            if self.left.name == 'expr':
                self.left = makeNodeFromExpr(self.left, typeName)
            else: #primitiveType or ArrayType
                self.left = TypeNode(self.left, typeName)
            # since a type might be mis-parsed as a name
            if self.left.__class__.__name__ == 'NameNode':
                self.left = TypeNode(self.parseTree.children[1], typeName)

        else:
            # manually induce left and right
            # for converting string concatenation to a method call
            # see ExprPrimaryNodes:ExprNode on string concat
            self.left = left
            self.right = right

        self.children.append(self.left)
        self.children.append(self.right)

    def disambigName(self):
        if self.left.__class__.__name__ != 'TypeNode':
            self.left.disambigName()
        self.right.disambigName()
        # helperDisambigName(self.left)
        # helperDisambigName(self.right)

    def checkType(self):
        self.left.checkType()
        self.right.disambigName()
        self.right.checkType()
        if (self.left.myType.isNum() and self.right.myType.isNum()) \
        or self.left.myType.assignable(self.right.myType) \
        or self.right.myType.assignable(self.left.myType):
            self.myType = self.left.myType
            return
        raise Exception("ERROR: Cannot cast type {} to type {}.".format(self.right.myType.name, self.left.myType.name))

    def codeGen(self):
        if hasattr(self, "code") and self.code != "":
            return
        self.code = ""

        # get object at right
        self.code += "; casting\n"
        self.right.codeGen()
        self.code += self.right.code

        # subtype test:

        # only test if not primitive
        if self.right.myType.isArray ==  self.left.myType.isArray:
            if (not self.right.myType.isPrimitive):
                # cast would've exception if array is casting to non-array and not Object
                offset = self.left.myType.typePointer.subTypeOffset
                self.code += p("mov", "ebx", "[eax]", "start subtype test") # access class tag of left object
                self.code += p("mov", "ebx", "[ebx + 4]") # access subtype testing column
                self.code += p("mov", "ebx", "[ebx + " + str(offset) + "]") # ebx has isSubtype

                # exception if not subtype, else do nothing (object is already in eax)
                self.code += p("cmp", "[G__Zero]", "ebx")
                self.code += p("je", "H__Throw_Exception")
        else: # one is array, one is Object/Serializable/Cloneable
            # make sure cast only one way
            if self.left.myType.isArray:
                self.code += p("jmp", "H__Throw_Exception")

        self.code += "; end of casting\n"

###################################################################################
# unqualCreate NEW name LPAREN args RPAREN
class ClassCreateNode(ASTNode):
    # always list all fields in the init method to show the class structure
    def __init__(self, parseTree, typeName):
        self.parseTree = parseTree
        self.className = TypeNode(parseTree.children[1], typeName)
        self.args = ArgsNode(parseTree.children[3], typeName)
        self.env = None
        self.children = [self.className, self.args]
        self.typeName = typeName
        self.cons = None # the constructor used to create the class

    def checkType(self):
        # return # TO REMOVE after name node type checking is done

        self.args.checkType()
        classDef = self.className.myType.typePointer
        # check class is not abstract
        if 'abstract' in classDef.mods:
            raise Exception('ERROR: Cannot create an instance of abstract class {}.'.format(self.className.myType.name))
        elif classDef.__class__.__name__ != 'ClassNode':
            raise Exception('ERROR: Cannot create an instance of {}, it is not a class.'.format(self.className.myType.name))

        # check 0 arguement constructor of superclass exists
        su = classDef.superClass
        while su != '':  # if it doesn't have an explict super class, its super class is java.lang.object, which is safe
            found = False
            for c in su.constructors:
                if c.params == []:
                    found = True
                    break
            if not found:
                raise Exception("ERROR: Class {} doesn't have a zero-arguement constructor.".format(su.name))
            su = su.superClass

        # get constructor using arg Types
        m = getMethod(classDef.constructors, "", self.args)
        if m:
            self.cons = m
            self.myType = self.className.myType
        else:
            raise Exception("ERROR: Class {} doesn't have a constructor with given argument types.".format(classDef.name))

        # check to make sure we are allowed to call this (protected?)
        # if self.cons is protected, check that:
        # - current class is in the same package
        if 'protected' in self.cons.mods:
            curClass = self.env.getNode(self.typeName, 'type')

            if curClass.packageName != classDef.packageName:
                raise Exception("ERROR: In class {0}, using a protected constructor, but class {1} is not in class {0}'s package ({2}).".format(curClass.name, classDef.name, curClass.packageName))

    def codeGen(self):
        if hasattr(self, "code"):
            return
        self.code = ""

        # 1. Allocating space for the object in heap
        classDef = self.className.myType.typePointer
        fieldOffset = classDef.fieldOffset
        numFields = len(fieldOffset)
        numBytes = (numFields + 1) * 4
        self.code += "; Creating an object for class " + self.typeName + "\n"
        self.code += p(instruction="mov", arg1="eax", arg2=numBytes, comment="allocating memory for object") + \
                     p(instruction="call", arg1="__malloc")
        # 2. Pointing first item to vtable
        self.code += importHelper(classDef.name, self.typeName, "C_"+classDef.name)
        self.code += p(instruction="mov", arg1="[eax]", arg2="dword C_" + classDef.name, comment="first item is vtable pointer")
        self.code += p("mov", "ebx", "eax", "store obj ptr at ebx")

        # 3. Calling constructor
        self.code += "; Calling constructor for object\n"

        # Evaluate arguments
        if self.args and hasattr(self.args, "codeGen"):
            if not hasattr(self.args, "code"):
                self.args.codeGen()
            self.code += self.args.code

        self.code += p(instruction="push", arg1="ebx", comment="pushing object as last argument")
        label = "M_" + self.cons.typeName + "_" + self.cons.name + "_" + self.cons.paramTypes
        self.code += importHelper(classDef.name, self.typeName, label)
        self.code += p(instruction="call", arg1=label, comment="Calling constructor")

        # 4. Popping parameters and pointer to object
        self.code += p(instruction="pop", arg1="eax", comment="eax now contains pointer to newly created object")
        self.code += p(instruction="add", arg1="esp", arg2=len(self.args.exprs)*4, comment="Popping parameters")

        self.code += ";End of object creation\n"


#################################################################################
# condOrExpr
class ExprNode(ASTNode):
    # always list all fields in the init method to show the class structure
    def __init__(self, parseTree, typeName):
        self.parseTree = parseTree
        self.left = None
        self.op = ''
        self.right = None  # another expr
        self.env = None
        self.children = []
        self.typeName = typeName # the type (class/interface) this node belongs under

        if parseTree.name == 'unaryNotPlusMinus' or parseTree.name == 'unaryExpr':
            self.op = parseTree.children[0].lex
            self.right = makeNodeFromExpr(parseTree.children[1], typeName)
        else:
            self.left = makeNodeFromExpr(parseTree.children[0], typeName)
            self.op = parseTree.children[1].lex
            self.right = makeNodeFromExpr(parseTree.children[2], typeName)

        self.children.append(self.left)
        self.children.append(self.right)

    def disambigName(self):
        if self.left:
            self.left.disambigName()
        self.right.disambigName()
        # helperDisambigName(self.left)
        # helperDisambigName(self.right)

    # use wrong name to stop method from being called until we finish other implemetation
    # def checkType(self):
    def checkType(self):
        # steps of type checking:
        #   check children's types (children's myType field will be populated with a typeStruct)
        #   check using the rule for current node
        #   make a TypeStruct node and populate myType field for self

        super().checkType() # check children's type first to populate their myType field

        # Unary operations:
        if not self.left:
            if self.op == '-' and self.right.myType.isNum():
                self.myType = TypeStruct("int", None)
                return
            elif self.op == '!' and self.right.myType.name == 'boolean':
                self.myType = self.myType = TypeStruct("boolean", None)
                return

        # Numeric types
        if self.left.myType.isNum() and self.right.myType.isNum():
            # Comparisons:
            if self.op in ['==', '!=', '<=', '>=', '>', '<']:
                self.myType = TypeStruct("boolean", None)
                return
            # numeric operations:
            elif self.op in ['+', '-', '*', '/', '%']:
                self.myType = TypeStruct("int", None)
                return
        # Boolean operations:
        if self.left.myType.name == 'boolean' and self.right.myType.name == 'boolean':
            if self.op in ['&&', '&', '|', '||', '!=', '==']:
                self.myType = TypeStruct("boolean", None)
                return

        if self.left.myType.assignable(self.right.myType) or self.right.myType.assignable(self.left.myType):
            if self.op == '==' or self.op == '!=' or self.op == 'instanceof':
                self.myType = TypeStruct("boolean", None)
                return

        # String concat:
        if ((self.left.myType.name =='java.lang.String' and self.right.myType.name not in ['void']) \
        or (self.right.myType.name =='java.lang.String' and self.left.myType.name not in ['void'])) and self.op == '+':
            self.myType = TypeStruct('java.lang.String', self.env.getNode('java.lang.String', 'type'))
            self.myType.link(self.env)

            # if left/right is a string, we don't need value of
            concatLeft = self.left
            concatRight = self.right

            # var1 + var2 === String.valueOf(var1).concat( String.valueOf(var2) )

            if self.left.myType.name != 'java.lang.String' or self.right.myType.name != 'java.lang.String':
                # methodInvoc name LPAREN args RPAREN (because it's static)
                # MethodInvNode
                # ID = NameNode
                #       - name = "String.valueOf"
                # args = ArgsNode
                #       - exprs = [self.left]

                # create MethodInvNode to call String.valueOf(left) and String.valueOf(right)
                # 1. Make NameNode for String.valueOf
                valueOfNameNode = NameNode(self.parseTree, True, self.typeName, "String.valueOf")
                valueOfNameNode.env = self.env

                # store these as they could change in step 2
                leftValueOf = self.left
                rightValueOf = self.right

                # 2. cast to Object if it is not safe to call String.valueOf
                safeTypes = ['java.lang.String', 'char', 'int', 'short', 'byte', 'boolean', 'java.lang.Object', 'String']

                if self.left.myType.name not in safeTypes or self.right.myType.name not in safeTypes:

                    # CastNode
                    # - left = TypeNode
                    #     - myType = TypeStruct
                    #         - name = 'java.lang.Object'
                    #         - typePointer = call link() !
                    # - right = value of thing being casted

                    # create object TypeNode
                    objectCastType = TypeNode('VOID', self.typeName)
                    objectCastType.myType = TypeStruct('java.lang.Object', None)
                    objectCastType.myType.link(self.env)

                    if self.left.myType.name not in safeTypes:
                        leftValueOf = CastNode(self.parseTree, self.typeName, objectCastType, self.left)

                    if self.right.myType.name not in safeTypes:
                        rightValueOf = CastNode(self.parseTree, self.typeName, objectCastType, self.right)

                # 3. call String.valueOf on the variable that needs to be converted
                if self.left.myType.name != 'java.lang.String':
                    leftArg = ArgsNode(self.parseTree, self.typeName, [leftValueOf])
                    concatLeft = MethodInvNode(self.parseTree, self.typeName, valueOfNameNode, leftArg)

                if self.right.myType.name != 'java.lang.String':
                    rightArg = ArgsNode(self.parseTree, self.typeName, [rightValueOf])
                    concatRight = MethodInvNode(self.parseTree, self.typeName, valueOfNameNode, rightArg)

            # methodInvoc primary PERIOD ID LPAREN args RPAREN
            # MethodInvNode
            # primary = self.left
            # ID = NameNode
            #       - name = "concat"
            # args = ArgsNode
            #       - exprs = [self.right]

            # create MethodInvNode to call left.concat(right)
            # 1. Make NameNode for String.concat (name = "concat")
            concatNameNode = NameNode(self.parseTree, True, self.typeName, "concat")
            # 2. Make ArgsNode for right
            rightArg = ArgsNode(self.parseTree, self.typeName, [concatRight])
            # 3. Put it all together
            self.concatMethodInv = MethodInvNode(self.parseTree, self.typeName, concatNameNode, rightArg, concatLeft)
            # 4. Check type to be safe
            self.concatMethodInv.checkType()

            return

        raise Exception("ERROR: Incompatible types. Left of {} type can't be used with right of {} type on operation {}".format(self.left.myType.name, self.right.myType.name, self.op))

    def codeGen(self):
        if hasattr(self, "code") and self.code != "":
            return
        self.code = ""

        # instanceOf
        if self.op == "instanceof":

            self.code += ("; evaluate instanceof\n")
            self.left.codeGen()
            self.code += self.left.code

            # only test if non-primitive, both arrays or both non arrays
            if self.right.myType.isArray == self.left.myType.isArray:
                if not self.left.myType.isPrimitive:
                    offset = self.right.myType.typePointer.subTypeOffset
                    self.code += p("mov", "eax", "[eax]") # access class tag of left object
                    self.code += p("mov", "eax", "[eax + 4]") # access subtype testing column
                    self.code += p("mov", "eax", "[eax + " + str(offset) + "]") # subType column stores 0 or 1 already

                else: # primitive type can be staticly evaluated
                    if self.right.myType.assignable(self.left.myType):
                        self.code += p("mov", "eax", "1")
                    else:
                        self.code += p("mov", "eax", "0")

            else: # non array is never an instance of array, array could be non array
                if self.right.myType.isArray:
                    self.code += p("mov", "eax", "0")
                else:
                    if self.right.myType.assignable(self.left.myType):
                        self.code += p("mov", "eax", "1")
                        n = getCFlowLabel()
                        endLabel = "_end" + n

                        self.left.codeGen()
                        self.code += self.left.code

                        self.code += p("cmp", "eax", "[G__Zero]")
                        self.code += p("mov", "eax", "1")
                        self.code += p("jne", endLabel)
                        self.code += p("mov", "eax", "0")
                        self.code += p(endLabel + ":", "")
                    else:
                        self.code += p("mov", "eax", "0")

            self.code += ("; end of instanceof\n")
            return

        # Unary operations:
        if not self.left:
            # get right output
            if not hasattr(self.right, "code"):
                self.right.codeGen()
            self.code += self.right.code

            if self.op == '-':
                self.code += p('neg', 'eax')
                return

            if self.op == '!':
                n = getCFlowLabel()
                endLabel = "_end" + n

                self.code += p("cmp", "eax", "[G__Zero]")
                self.code += p("mov", "eax", "1")
                self.code += p("je", endLabel)
                self.code += p("mov", "eax", "0")
                self.code += p(endLabel + ":", "")
                return

        # Comparisons that short-circuit:
        if self.op in ['&&', '||']:
            n = getCFlowLabel()
            endLabel = "_end" + n

            # get left code
            if not hasattr(self.left, "code"):
                self.left.codeGen()
            self.code += self.left.code

            if self.op == '&&':
                # if left result == false, break out
                self.code += p("cmp", "eax", "0")
            elif self.op == '||':
                # if left result == true, break out
                self.code += p("cmp", "eax", "1")

            self.code += p("je", endLabel, "", " breaking out of " + self.op)

            # get right code
            if not hasattr(self.right, "code"):
                self.right.codeGen()
            self.code += self.right.code

            self.code += p(endLabel + ":", "")
            return
        if self.op in ['==', '!=', '<=', '>=', '>', '<']:
            n = getCFlowLabel()
            falseLabel = "_false" + n
            endLabel = "_end" + n

            self.code += self.getIfFalse(falseLabel)
            self.code += p("mov", "eax", "1")
            self.code += p("jmp", endLabel)
            self.code += p(falseLabel + ":", "")
            self.code += p("mov", "eax", "0")
            self.code += p(endLabel + ":", "")
            return

        # String Add
        if (self.left.myType.name =='java.lang.String' or self.right.myType.name =='java.lang.String') and self.op == '+':
            # ( String.valueOf(right) ).concat( left )
            if not hasattr(self.concatMethodInv, "code"):
                self.concatMethodInv.codeGen()
            self.code += self.concatMethodInv.code
            return

        # is binary and rest can use helper function:
        self.code += self.codeGenLeftRight()

        # now:
        # ebx = left result
        # eax = right result
        # so, basically do "eax = ebx op eax"

        # Comparisons that don't short-circuit:
        if self.op in ['|', '&']:
            n = getCFlowLabel()
            endLabel = "_end" + n

            if self.op == '|':
                # if right = False, then we output left's result
                self.code += p("cmp", "eax", "1")
            elif self.op == '&':
                # if right = True, then we output left's result
                self.code += p("cmp", "eax", "0")

            self.code += p("je", endLabel)
            self.code += p("mov", "eax", "ebx")
            self.code += p(endLabel + ":", "")
            return

        # Binary operations:

        # Number Add, Subtract, Multiply
        if self.op in ['+', '-', '*']:
            # operation -> generated code
            ops = {
                '+': "add",
                '-': "sub",
                '*': "imul"
            }
            self.code += p(ops[self.op], "ebx", "eax", " left:ebx " + self.op + " right:eax")
            self.code += p("mov", "eax", "ebx", " move result to eax")
            return

        # Divide, Modulus
        if self.op in ['/', '%']:
            # switch eax and ebx, because "idiv ebx" -> eax = edx:eax / ebx
            self.code += p('xchg', 'eax', 'ebx')

            # divide by 0 check
            if self.op == '/':
                self.code += p("cmp", "ebx", "0")
                self.code += p("je", "H__Throw_Exception")

            self.code += p('push', 'edx') # save edx in case someone else was using it
            self.code += p('cdq', '', None, " set edx to the sign of eax")
            self.code += p("idiv", "ebx", "", " left:eax " + self.op + " right:ebx")

            # eax = eax / abx
            # edx = eax % ebx
            if self.op == '%':
                self.code += p("mov", "eax", "edx", " move quotient result to eax") # quotient is in edx

            self.code += p('pop', 'edx') # restore edx
            return


    # generate shorter code if self.op = comparison
    def getIfFalse(self, label):
        if self.op not in ['==', '!=', '<=', '>=', '>', '<']:
            self.codeGen()
            return self.code + p("cmp", "eax", "[G__Zero]") + p("je", label)
        else:
            result = self.codeGenLeftRight()
            result +=  p("cmp", "ebx", "eax", "left " + self.op + " right")
            if self.op == '==':
                result += p("jne", label) # jump if false (not equal)
            elif self.op == '!=':
                result += p("je", label)  # jump if false (equal)
            elif self.op == '>':
                result += p("jle", label) # jump if false (le)
            elif self.op == '<':
                result += p("jge", label) # jump if false (ge)
            elif self.op == '>=':
                result += p("jl", label) # jump if false (lt)
            elif self.op == '<=':
                result += p("jg", label) # jump if false (gt)
            return result


    # helper for codeGen for getting code from left and right
    def codeGenLeftRight(self):
        result = ""
        # get left output
        if not hasattr(self.left, "code"):
            self.left.codeGen()
        result += self.left.code
        result += p("push", "eax") # left result onto stack

        # get right output
        if not hasattr(self.right, "code"):
            self.right.codeGen()
        result += self.right.code
        result += p("pop", "ebx") # left output into ebx

        return result

    # returns True, False, Int or None (for non-constant expr)
    # children of exprNode is either exprNode or literalNode
    def getConstant(self):
        if not hasattr(self.right, "getConstant"):
            return None
        cRight = self.right.getConstant()
        if cRight == None:
            return None

        # Unary Ops
        if not self.left:
            if self.op == '-':
                return -cRight
            return not cRight # op = '!'

        else:
            if not hasattr(self.left, "getConstant"):
                return None
            cLeft = self.left.getConstant()
            if cLeft == None:
                return None

            # arithmetic
            if self.op == '+':
                return cLeft + cRight
            elif self.op == '-':
                return cLeft - cRight
            elif self.op == '*':
                return cLeft * cRight
            elif self.op == '/':
                return cLeft // cRight
            elif self.op == '%':
                return cLeft % cRight
            # Comparison
            elif self.op == '==':
                return cLeft == cRight
            elif self.op == '!=':
                return cLeft != cRight
            elif self.op == '>':
                return cLeft > cRight
            elif self.op == '<':
                return cLeft < cRight
            elif self.op == '>=':
                return cLeft >= cRight
            elif self.op == '<=':
                return cLeft <= cRight
            # boolean Ops
            elif self.op == '&&' or self.op == '&':
                return cLeft and cRight
            elif self.op == '||' or self.op == '|':
                return cLeft or cRight
            else:
                return None

###################################################################################
# fieldAccess primary PERIOD ID
# Could also just be a COMPID
class FieldAccessNode(ASTNode):
    # always list all fields in the init method to show the class structure
    def __init__(self, parseTree, typeName, compid=False):
        self.parseTree = parseTree
        self.primary = ''
        self.ID = '' # field name
        self.env = None
        self.children = []
        self.typeName = typeName # the type (class/interface) this node belongs under
        self.isLength = False
        if not compid:
            # input: fieldAccess primary PERIOD ID
            self.primary = makeNodeFromAllPrimary(parseTree.children[0], typeName)
            self.ID = NameNode(parseTree.children[2], False, typeName)
            if self.ID.name == "length":
                self.isLength = True
            self.children.append(self.primary)
        else:
            # input: COMPID
            self.ID = NameNode(parseTree, False, typeName)

        self.children.append(self.ID)

    def disambigName(self):
        if not self.primary: # this implies that the ID has nothing that comes before it
            # helperDisambigName(self.ID)
            self.ID.disambigName()
            # self.right.disambigName()
        else:
            self.primary.disambigName()

    def checkType(self):
        if self.primary:
            self.primary.checkType()
            if self.primary.myType.isArray or self.primary.myType.isPrimitive:
                self.ID.prefixLink = self.primary
            else:
                self.ID.prefixLink = self.primary.myType.typePointer
        self.ID.checkType()
        self.myType = self.ID.myType

        # check protected
        try:
            if "protected" in self.ID.prefixLink.mods:
                checkProtected(self.ID.prefixLink, self)
        except: # where there are no mods
            return

    def getIfFalse(self, label):
        self.codeGen()
        return self.code + p("cmp", "eax", "[G__Zero]") + p("je", label)

    # generates code that evaluates the address of field access
    # result stored in eax (address)
    def addr(self):
        result = ""
        if not self.primary:
            return self.ID.addr()

        else:
            result += "; Start of calculating address for non-static field\n"
            self.primary.codeGen()
            result += self.primary.code
            # Null check
            # At this point, eax stores the address of the object
            result += p(instruction="call", arg1="H__Null_Check", comment="calling null check function")
            # Make eax store the address to the field we're accessing
            if self.isLength:
                result += p(instruction="add", arg1="eax", arg2=4, comment="calculating pointer to length")
            else:
                result += p(instruction="add", arg1="eax", arg2=self.ID.prefixLink.offset, comment="calculating pointer to the field")
            result += "; End of calculating address for non-static field\n"

        return result


    def codeGen(self):
        if hasattr(self, "code"):
            return

        fieldNode = self.ID.prefixLink
        self.code = "; Accessing a field \n"
        # Evaluating the address of the field we're trying to access
        if self.primary:
            self.code += self.addr()
            self.code += p(instruction="mov", arg1="eax", arg2="[eax]")

        else:
            self.ID.codeGen()
            self.code += self.ID.code
        self.code += "; End of field access\n"




###################################################################################
# methodInvoc
class MethodInvNode(ASTNode):
    # always list all fields in the init method to show the class structure
    def __init__(self, parseTree, typeName, ID = '', args = None, primary = None):
        self.parseTree = parseTree
        self.primary = None  # can be empty
        self.ID = '' # can be either ID or compID
        self.args = None
        self.env = None
        self.children = []
        self.method = None
        self.typeName = typeName # the type (class/interface) this node belongs under

        if ID == '' or args is None:
            # input parse tree is either: methodInvoc primary PERIOD ID LPAREN args RPAREN
            #  methodInvoc name LPAREN args RPAREN
            self.ID = NameNode(parseTree.children[-4], True, typeName)
            self.args = ArgsNode(parseTree.children[-2], typeName)
            if parseTree.children[0].name == 'primary':
                self.primary = makeNodeFromAllPrimary(parseTree.children[0], typeName)
            self.override = False
        else:
            # manually induce ID and args
            # for converting string concatenation to a method call
            # see ExprPrimaryNodes:ExprNode on string concat
            self.ID = ID
            self.args = args
            if primary is not None:
                self.primary = primary
            self.override = True

        self.children.append(self.primary)
        self.children.append(self.args)
        self.children.append(self.ID)

    def disambigName(self):
        if not self.primary: # this implies our ID doesn't have anything that comes before it
            if isinstance(self.ID, NameNode) and len(self.ID.IDs) > 1:
                self.ID.disambigName()
                # helperDisambigName(self.ID)
        if self.args:
            self.args.disambigName()

    def checkType(self):
        # steps of type checking:
        #   check param's types
        #   check using the rule for current node
        #   make a TypeStruct node for self (return type of method)

        # populate params myTypes
        for param in self.args.exprs:
            param.checkType()

        # now that we have the method name, param types, we need to:
        # - check if method exists under the class its under

        m = None
        if not self.primary:
            self.ID.checkType()
            m = getMethod(self.ID.prefixLink.values(), self.ID.methodName, self.args)
            self.methodClass = self.ID.methodClass
        else:
            self.primary.checkType()
            methods = []
            methods.extend(self.primary.myType.typePointer.methods)
            methods.extend([meth for meth in self.primary.myType.typePointer.inherits if isinstance(meth, MemberNodes.MethodNode)]) # need to check inherited methods as well
            m = getMethod(methods, self.ID.name, self.args)
            self.methodClass = self.primary.myType

        if m:

            # check static
            if (not self.ID.shouldBeStatic and not self.override) and 'static' in m.mods:
                raise Exception("ERROR: Non-static access of static method {}.".format(m.name))

            # check protected
            if "protected" in m.mods:
                checkProtected(m, self)

            self.method = m
            self.myType = m.methodType.myType
            return
        else:

            raise Exception("ERROR: Class {} doesn't have a method {} with given argument types.".format(self.typeName, self.ID.name))

    def reachCheck(self, inMaybe):
        if not inMaybe:
            raise Exception("ERROR: not reaching a variable declaration statement for var {}".format(self.name))
        self.outMaybe = inMaybe

    def evalArgs(self):
        # Evaluate arguments
        if self.args and hasattr(self.args, "codeGen"):
            if not hasattr(self.args, "code"):
                self.args.codeGen()
            self.code += self.args.code

    def getIfFalse(self, label):
        self.codeGen()
        return self.code + p("cmp", "eax", "[G__Zero]") + p("je", label)

    def codeGen(self):
        if hasattr(self, "code"):
            return
        self.code = ""
        # Static methods
        if "static" in self.method.mods:
            # 1. Prologue
            mLabel = "M_" + self.method.typeName + "_" + self.method.name + "_" + self.method.paramTypes
            (pro, epi) = genMethodInvoke(mLabel)
            self.code += pro

            # 2. Evaluating arguments
            self.evalArgs()

            # 3. Calling static method
            self.code += importHelper(self.method.typeName, self.typeName, mLabel)
            self.code += p(instruction="call", arg1=mLabel, comment="calling method")

            # 4.  Popping off all the arguments
            toPop = 0
            if self.args:
                toPop = len(self.args.exprs)*4
            self.code += p(instruction="add", arg1="esp", arg2=toPop, comment="popping off arguments")

            # 5. Epilogue
            self.code += epi
        # Non static methods
        else:

            # 1. Prologue (saving caller-save registers)
            (pro, epi) = genMethodInvoke(self.method.name + "_" + self.method.paramTypes)
            self.code += pro

            # 2. Evaluate arguments and pushing them on the stack (handled by the args node)
            self.evalArgs()

            # 3. Evaluating o, the object
            if self.primary:
                if not hasattr(self.primary, "code"):
                    self.primary.codeGen()
                self.code += self.primary.code
            else: # a name node
                if not hasattr(self.ID, "code"):
                    if not self.ID.prefixNodes:
                        cNameNode = genNameNode(self.typeName)
                        cNameNode.isThis = True
                        self.ID.prefixNodes.append(cNameNode)
                    self.ID.codeGen()
                self.code += self.ID.code

            # 4. Null check
            # At this point, eax stores the address of the object
            self.code += p(instruction="call", arg1="H__Null_Check", comment="calling null check function")

            # 5. Pushing the object pointer on the stack
            self.code += p(instruction="push", arg1="eax", comment="pushing object pointer to the stack")


            # 6. Fetch vtable
            toPop = 0
            if self.args:
                toPop = len(self.args.exprs)*4
            self.code += p(instruction="mov", arg1="eax", arg2="[eax]", comment="get vtable")


            # 7. Fetching the address of m's implementation
            mOffset = self.method.methodOffset
            if self.methodClass.__class__.__name__ == "InterNode":
                self.code += p(instruction="mov", arg1="eax", arg2="[eax]", comment="SIT column")
            self.code += p(instruction="mov", arg1="eax", arg2="[eax+"+str(mOffset)+"]", comment="get address of method impl")

            # 8. Calling the method
            self.code += p(instruction="call", arg1="eax", comment="calling method")

            # 9. Popping off arguemnts and object off stack
            self.code += p(instruction="add", arg1="esp", arg2=toPop+4, comment="pop off args and object")

            # 10. Epilogue
            self.code += epi

        # All children's code gen is already called





################# Helper #######################

def getMethod(methods, methodName, args):
    # methodName = "" if it's constructor
    for c in methods:
        if (methodName == "" or c.name == methodName) and len(args.exprs) == len(c.params):
            found = True
            for i, param in enumerate(args.exprs):
                if c.params[i].paramType.myType != param.myType:
                    found = False
            if found:
                return c
    return None
