from CodeGenUtils import p, importHelper
# since nameNode is a mess, a simple class for the purpose of codeGen nameNode
class genNameNode():
    def __init__(self, typeName):
        self.isThis = False
        self.isStatic = False
        self.isLength = False
        self.nodeLink = None
        self.typeName = typeName # which type this node resides in
        self.code = ""

    # returns code
    def codeGen(self):
        if self.isThis:
            self.code = p("mov", "eax", "[ebp + 8]")
        elif self.isLength:
            # object will already be in eax
            # Null check
            self.code += p("call", "H__Null_Check")
            self.code += p("add", "eax", 4)
            self.code +=  p("mov", "eax", "[eax]")
        elif self.nodeLink.__class__.__name__ == "VarDclNode":
            if self.nodeLink.isField :
                self.code = p("mov", "eax", "[ebp + 8]")
                self.code += p("call", "H__Null_Check")
                self.code += p("add", "eax", self.nodeLink.isField.offset)
                self.code +=  p("mov", "eax", "[eax]")
            else:
                self.code = p("mov", "eax", "[ebp - " + str(self.nodeLink.offset) + "]")
        elif self.nodeLink.__class__.__name__ == "ParamNode":
            self.code = p("mov", "eax", "[ebp + " + str(self.nodeLink.offset) + "]")
        elif self.nodeLink.__class__.__name__ == "FieldNode":
            if self.isStatic:
                label = self.nodeLink.typeName + "_" + self.nodeLink.name
                self.code += importHelper(self.nodeLink.typeName, self.typeName, "S_"+label) + \
                            p("mov", "eax", "dword S_"+label)
                self.code += p("mov", "eax", "[eax]")
            else:
                # object will already be in eax
                # Null check
                self.code += p("call", "H__Null_Check")
                self.code += p("add", "eax", self.nodeLink.offset)
                self.code +=  p("mov", "eax", "[eax]")
        return self.code

    def addr(self):
        result = ""
        if self.isThis:
            result = p("mov", "eax", "ebp")
            result += p("add", "eax", "8")
        # Object will already be in eax
        elif self.isLength:
            result += p("call", "H__Null_Check")
            result += p("add", "eax", 4)
        elif self.nodeLink.__class__.__name__ == "VarDclNode":
            if self.nodeLink.isField :
                result = p("mov", "eax", "[ebp + 8]")
                result += p("call", "H__Null_Check")
                result += p("add", "eax", self.nodeLink.isField.offset)
            else:
                result = p("mov", "eax", "ebp")
                result += p("sub", "eax", str(self.nodeLink.offset))
        elif self.nodeLink.__class__.__name__ == "ParamNode":
            result = p("mov", "eax", "ebp")
            result += p("add", "eax", str(self.nodeLink.offset))
        elif self.nodeLink.__class__.__name__ == "FieldNode":
            if self.isStatic:
                label = self.nodeLink.typeName + "_" + self.nodeLink.name
                result += importHelper(self.nodeLink.typeName, self.typeName, "S_"+label) + \
                            p("mov", "eax", "dword S_"+label)
            else:
                # object will already be in eax
                result += p("call", "H__Null_Check")
                result += p("add", "eax", self.nodeLink.offset)
        return result
