public class J1_00_Field_Basic_Static_Fields {
    public static int f1;
    public static int f2;
    public static int f3 = 4;
    public J1_00_Field_Basic_Static_Fields(){}
    public static int test() {
        return J1_00_Field_Basic_Static_Fields.test2();
    }

    public static int test2(){
        return J1_00_Field_Basic_Static_Fields.f3;
    }
}