// CODE_GENERATION
// expected result 42 + 2 + 42 + 5 = 91
import pkg.*;

public class Main {
	public Main() {}
	public int j = 5;

	public static int test() {
		Interface a = new pkg.Class();
		Object o = a.clone();
		return o.hashCode()  + a.k(2);
	}
}
