//  test case for For While step 1
// Expected output 106
// public class J1_01_ForWhile {
//     public static int test() {
//         int i = 1;
//         while ( !(i > 39)){
//           for (int j = 9; j > 7; j = j - 1){
//             i = i + j;
//           }
//           i = i * 2;
//         }
//         return i;
//     }
// }
public class J1_01_ForWhile {
    public static int test() {
      int i = 0;
      int j = 0;
      while ( !(i == 10)){
          i = i + 2;
          j = j + 1;
      }
      return j;
    }
}
