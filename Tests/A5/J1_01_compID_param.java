public class J1_01_compID_param {

    public J1_01_compID_param() {
      j = 5;
    }
    public int i = 4;
    public int j = 1;
    public static int n = 10;

    public int m(J1_01_compID_param p){
      p.i = p.i + 2;
      return j + p.i;
    }

    public static int test() {
      J1_01_compID_param k = new J1_01_compID_param();
      return k.i + k.m(k) + J1_01_compID_param.n;
    }
}
