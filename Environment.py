import string

class Env:
    nodeToNamespace = dict({
        'FieldNode': 'fieldDcl',
        'InterNode': 'type',
        'ClassNode': 'type',
        'MethodNode': 'method',
        'VarDclNode': 'expr',
        'ParamNode': 'expr'

    })

    def __init__(self, parentEnv):
        self.parentEnv = parentEnv
        self.map = {}
                #  entries in the map:
                # type: (typeName, 'type') -> node
                # method: (methodName. 'method') -> {paramTypes -> node} for method overloding
                # expr: (name, 'expr') -> node

    def addtoEnv(self, node):
        # use node.__class__.__name__ to get namespace
        nodeType = node.__class__.__name__
        if nodeType not in self.nodeToNamespace:
            namespace = 'expr'
        else:
            namespace = self.nodeToNamespace.get(nodeType)

        # Add to Env
        key = (node.name, namespace)
        # add entry in nested map for method overloding
        if namespace == 'method':
            if key not in self.map:
                self.map[key] = {}
            self.map[key][node.paramTypes] = node
        else:
            if key in self.map:
                raise Exception('ERROR: Declaration of {} is already in current Environment'.format(node.name))
            self.map[key] = node


    def getNode(self, name, namespace):
        key = (name, namespace)

        if key in self.map:
            return self.map.get(key)
        elif self.parentEnv:
            return self.parentEnv.getNode(name, namespace)
        raise Exception("ERROR: Can't find definition of {} in the Environment".format(name))

    # Finding a node in the current environment. Terminates before reaching the global Env (see compEnv implementation)
    def findNode(self, name, namespace):
        key = (name, namespace)
        if key in self.map:
            return self.map[key]
        elif self.parentEnv:
            return self.parentEnv.findNode(name, namespace)
        return None



###################################

class GlobalEnv(Env):
    def __init__(self):
        super().__init__(None)
        #self.map = {} # {Canonical name -> node}
        self.packageMap = {} # {packageName -> {(simpleName, namespace) -> node}}
                            #   A map of maps
                            #   contains duplicate info as self.map for easy searching

    # input: Compiliation Unit Node (CompNode)
    def addtoEnv(self, node):
        pName = node.packageName
        typeDcl = node.typeDcl
        typeDclType = typeDcl.__class__.__name__
        # at global level there are only class and interface
        namespace = self.nodeToNamespace.get(typeDclType)

        # Add to map
        mapKey = pName + '.' + typeDcl.name
        if not pName:
            mapKey = typeDcl.name
        if mapKey in self.map:
            raise Exception('ERROR: Declaration of {} is already in current Environment'.format(mapKey))
        self.map[mapKey] = typeDcl

        # Add to packageMap
        pMapKey = (typeDcl.name, namespace)
        if not pName in self.packageMap:
            self.packageMap[pName] = {}

        if pMapKey in self.packageMap[pName]:
            raise Exception('ERROR: Declaration of {} is already in current Environment'.format(mapKey))
        self.packageMap[pName][pMapKey] = typeDcl

    def weedGlobalEnv(self):
        # call after building global Env to resolve clashes:
        # TODO crashing some A2 tests

        # from pprint import pprint
        # print('self.map')
        # pprint(self.map)
        # print('self.packageMap')
        # pprint(self.packageMap)
        # pprint(self.packageMap.keys())

        for i in self.map.keys():
            for j in self.packageMap.keys():
                iNode = self.map.get(i)
                # if j == "java.util.ArrayList.foo" and "java.util.ArrayList.foo" in i:
                #     print('j ', j)
                #     print('i ', i)
                #     print('iNode.packageName ', iNode.packageName)
                # i.rpartition('.')[0] == j
                if i in j and iNode.packageName:
                    # print('i ({}) is in j ({})'.format(i,j))
                    raise Exception('ERROR: Package name {} clashing with Type name {}'.format(j, i))

    # Use getNode() from base class to get node using Canonical Name (full name)
    def getNode(self, key, imported, packageName):
        name = key[0]
        found = [] # list of found nodes

        # 1. enclosing class/interface
        #    - already did

        # fully qualified name (canonName)
        if name in self.map:
            return self.map.get(name)

        # 2. single-type import (simple Name)
        for i in imported:
            if not '*' in i:
                full = i.split(".")[-1]
                if full == name:
                    found.append(self.map.get(i))

        # 3. type in the current package
        full = packageName + '.' + name
        if full in self.map:
            return self.map.get(full)

        # 4. import-on-demand
        checked = []
        for i in imported:
            if '*' in i and i not in checked:
                checked.append(i)
                full = i.replace("*", name)
                if full in self.map:
                    found.append(self.map.get(full))

        # imported implicitly: java.io.*, java.lang.*, java.util.*
        implicitly = ['java.lang.']

        # remove from implicitly if we are in that package already
        if (packageName + '.') in implicitly:
            implicitly.remove(packageName + '.')
        
        # remove from implicitly if we explicitly imported it
        for i in imported:
            if '*' in i and i.replace("*", "") in implicitly:
                implicitly.remove(i.replace("*", ""))

        for i in implicitly:
            if (i+name) in self.map:
                found.append(self.map.get(i+name))

        if len(found) == 1:
            return found[0]
        elif len(found) > 1:
            raise Exception("ERROR: Trying to access ambiguous name '{}' in the Environment".format(key))

        raise Exception("ERROR: Can't find definition of {} in the Environment".format(key))

    # method for getting all the nodes under a package (import All)
    # returns a dict of types under that package name
    def ExistPackage(self, pName):
        p = pName.split(".")
        for k in self.packageMap.keys(): #check if package Name is a prefix
            kk = k.split(".")
            if len(kk) < len(p):
                continue
            found = True

            for i in range(len(p)):
                if kk[i] != p[i]:
                    found = False
                    break
            if found:
                return
        raise Exception("ERROR: Can't find definition of package {} in the Environment".format(pName))

###################################

class CompEnv(Env):
    def __init__(self, parentEnv, imported, packageName):
        super().__init__(parentEnv) # globalEnv is parentEnv
        self.imported = imported # list of strings that have been imported in this comp
        self.packageName = packageName # string of the current file's package name

    def getNode(self, name, namespace):
        key = (name, namespace)
        if key in self.map:
            return self.map.get(key)
        elif self.parentEnv: # globalEnv
            return self.parentEnv.getNode(key, self.imported, self.packageName)
        raise Exception("ERROR: Can't find definition of {} in the Environment".format(name))

    # To handle edge cases for finding expr in the environment, we terminate finding expr (local vars) at compEnv (so it doesn't traverse to the global env)
    def findNode(self, name, namespace):
        key = (name, namespace)
        if key in self.map:
            return self.map[key]
        else:
            return None
