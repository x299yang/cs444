
# format a string that represents aseembly code for any instruction
# String, String, String/None, String/None
def p(instruction, arg1, arg2="", comment=""):
    result = "      " + instruction + " " + str(arg1)
    if arg2 or arg2 == 0:
        result += ", " + str(arg2)
    if comment:
        result += "      ;" + comment
    return result + "\n"

# format a string that represents label
# String, "class"/"method"/"field"/"constant"/"local/vtable/"srarix"/"helper","globalConstant", "array"
# helper: useful helper functions (these ARE NOT METHODS)
#   (label list to be extended)
#   (local label are just for local running of the code)
used_labels = set()
local_labels = 0
control_flow_label = 0
literal_string_label = 0

def getCFlowLabel():
    global control_flow_label
    control_flow_label += 1
    return str(control_flow_label)

def getLStringLabel():
    global literal_string_label
    literal_string_label += 1
    return str(literal_string_label)

def pLabel(name, type, comment=""):
    global local_labels
    global used_labels
    C = "" # Used for checking if lable is used
    l = "" # The result label
    if type == "local":
        C = "_" + str(local_labels)
        local_labels += 1
        l = type[0].upper() + "_" + name
    else:
        if type == "constant":
            l = "cc_" + name
        else :
            l = type[0].upper() + "_" + name
        C = l
        # if l in used_labels:
        #     raise("ERROR: Generated repeated label " + l)
        used_labels.add(C)

    result = ""
    if type in ["class", "method", "static", "helper", "globalConstant", "array"]: # make global
        result += "global " + l + "\n"
    if comment:
        result += l + ":            ;" + comment + "\n"
    else:
        result += l + ":            ;" + type + " label\n"

    return result

# adds procedure pro/epilogues around the content
def genProcedure(content, comment):
    com = " ; Start of a procedure: "
    if comment:
        com += comment + "\n"
    else:
        com += "\n"
    pro = p("push", "ebp", None, None) \
    + p("mov", "ebp", "esp", None) \
    + p ("sub", "esp", "12", None) \
    + p ("push", "ebx", None, "push callee-save regs") \
    + p ("push", "esi", None, None) \
    + p ("push", "edi", None, None)

    epi = p ("pop", "edi", None, "pop callee-save regs") \
    + p ("pop", "esi", None, None) \
    + p ("pop", "ebx", None, None) \
    + p("mov", "esp", "ebp", None) \
    + p("pop", "ebp", None, None) \
    + p ("ret", "", None, None)

    return com + pro + content + epi + " ; End of a procedure\n"

# Adds method invoke prologue and eilogie around the content
def genMethodInvoke(method):
    pro = ";Calling a method " + method + "\n" + \
          p(instruction="push", arg1="ecx", comment="saving caller-save reg ecx") + \
          p(instruction="push", arg1="edx", comment="saving caller-save reg edx")


    epi = p(instruction="pop", arg1="edx", comment="restoring calleer-save reg edx") + \
          p(instruction="pop", arg1="ecx", comment="restoring caller-save reg ecx") + \
           "; End of method invocation\n"

    return (pro, epi)

# generates shorter code for constant value conditionals and comparison conditionals
# cond is either literalNode or ExprNode or NameNode
# cond could also be methodInvNode, arrayAccessNode or fieldAccessNode
def iffalse(cond, label):
    result = ""
    if cond.__class__.__name__ == "NameNode":
        cond.codeGen()
        result += cond.code
        result += p(instruction="cmp", arg1="eax", arg2=0, comment="comparing eax with 0")
        result += p(instruction="je", arg1=label)
        return result

    val = cond.getConstant()
    if val != None:
        if val == True:
            result = ""   # no jump if true
        elif val == False:
            result += p("jmp", label)
        # else val == Int shouldn't happen here, since we already done typeCheck
    else:
        result = cond.getIfFalse(label)

    return result

# Generates generic (not specific to class) helper functions
def genericHelperFunctions():

        code = ""
        # The Zero constant
        code += pLabel(name="_Zero", type="globalConstant", comment="The zero constant used for null checks")
        code += p(instruction="dd", arg1=0)

        # Helper function to throw exceptions
        code += pLabel(name="_Throw_Exception", type="helper", comment="helper function for throwing exceptions") + \
                p(instruction="call", arg1="__exception", comment="calls the exception function") + \
                ";End function for throwing exceptions\n"


        # Helper function to perform a null check
        # Note: using jle instead of calling the function since we don't need to preserve eip upon calling the exception function (?)
        #       saving and restoring ebx, a callee-save register
        code += pLabel(name="_Null_Check", type="helper", comment="helper function for null check") + \
                p(instruction="cmp", arg1="eax", arg2="[G__Zero]") + \
                p(instruction="jle", arg1="H__Throw_Exception") + \
                p(instruction="ret", arg1="")

        # Helper function to perform bounds check on array
        # eax = pointer to array
        # ecx = index i
        code += pLabel(name="_Bounds_Check", type="helper", comment="helper function to check array bounds") + \
                p(instruction="push", arg1="ebx", comment="saving ebx") + \
                p(instruction="mov", arg1="ebx", arg2="[eax+4]", comment="ebx stores length of array") + \
                p(instruction="cmp", arg1="ecx", arg2="ebx") + \
                p(instruction="jge", arg1="H__Throw_Exception") + \
                p(instruction="pop", arg1="ebx", comment="restoring ebx") + \
                p(instruction="ret", arg1="")

        return code

# Generates code to import helper generic functions and global constants
def globalImport(genGlobalFunction, typeName):
    code = "; Function to import helper functions and constants\n" + \
           pLabel(name=typeName + "_globalImport", type="helper") + \
           p(instruction="extern", arg1="__malloc") + \
           p(instruction="extern", arg1="__exception")

    if not genGlobalFunction:
        code +=  p(instruction="extern", arg1="G__Zero") + \
                 p(instruction="extern", arg1="H__Throw_Exception") + \
                 p(instruction="extern", arg1="H__Null_Check") + \
                 p(instruction="extern", arg1="H__Bounds_Check")

    code += p(instruction="ret", arg1="")
    code += "; End of importing helper functions and constants\n"
    return code

# Helper function for importation of label (only import if it's not already inside the same .s file)
def importHelper(targetClass, ownClass, labelName):
    if targetClass != ownClass:
        return p(instruction="extern", arg1=labelName, comment="importing label")
    return ""

# Helper function for allocating space on the heap
def heapAllocate(size):
    code = p(instruction="mov", arg1="eax", arg2=size, comment="size of field in bytes") + \
           p(instruction="call", arg1="__malloc", comment="allocating space on heap for the field")
    return code


####################################################
class codeSectionUtil():
    def __init__(self, info):
        self.info = info
        self.result = "; Start of code section for " + info + "\n"

    def end(self):
        self.result += "; End of code section for " + self.info + "\n"
        return self.result

# Test
# i = codeSectionUtil("test")
# i.result += pLabel("start", "class")
# i.result += p("mov", "eax", "1", None)
# print(i.end())
