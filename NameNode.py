from AST import ASTNode, getParseTreeNodes
from TheTypeNode import TypeStruct, getSupers
import MemberNodes
import TypeNodes
from Environment import Env
from CodeGenUtils import p
from codeGenNodes import genNameNode

# name nodes: contains compID and IDs

# TODO:
# Optional stuff:
# clean up findNode vs getNode (remove findNode if getNode works properly)[optional]
# if sth is None in line Node, don't append to children
# add comments for what is self.typeName at each class (the type name that this node is underneath)
# Necessary:
# 2) checking if remaining compid are instance fields
# a) check if the last ID in the name is a method if methodInvoke == true (might not be necessary) [this is after type checking]


class NameNode(ASTNode):
    def __init__(self, parseTree, methodInvoke, typeName, name = ""):

        self.parseTree = parseTree
        self.name = "" # str: stores original lex of the name (ID/COMPID)
        self.IDs = [] # a queue of strings: each string is an ID in a COMPID/ID.
        self.prefix = "" # str: stores the currently resolved/identified prefix in the name
        self.prefixLink = None # 3 possible values:
                                # 1) a pointer to an AST node (i.e. the prefix is resolved)
                                # 2) "contain" (prefix isn't resolved but is identified)
                                # 3) None: neither of 1 or 2
        self.methodInvoke = methodInvoke # Bool: true if the last ID in the name is a method invokation
        self.methodName = ""
        self.env = None
        self.typeName = typeName # the name of the class or interface that this node belongs under
        self.children = []
        self.myType = None # will become TypeStruct to tell us what the whole is/returns
        self.shouldBeStatic = False
        self.pointToThis = False
        self.staticField = None
        self.prefixNodes = [] # stores the past prefix links so we can evaluate them one by one

        if name == "":
            self.name = getParseTreeNodes(["ID", "COMPID"], parseTree)[0].lex
        else:
            # manually induce name
            # for converting string concatenation to a method call
            # see ExprPrimaryNodes:ExprNode on string concat
            self.name = name

        self.IDs = self.name.split(".")

    # Updates the resolved/identified prefix
    # ASSUMPTION: will never be called when self.IDs is empty
    def addToPrefix(self, node):
        # If self.prefixLink is contain, that means our self.prefix wasn't resolved yet
        if type(self.prefixLink) is str and self.prefixLink == "contain":
            self.prefixLink = node
            return

        # Otherwise, we update both self.prefix and self.prefixLink, since we have now resolved a new ID
        if self.prefix:
            self.prefix += "."
        self.prefix += self.IDs[0]
        self.prefixLink = node
        self.IDs.pop(0)

    # Checks and updates prefix if the next ID in self.IDs is a "this" pointer
    def checkThis(self):
        if not self.IDs:
            return True

        # The scanner makes sure that "this" can only be in the beginning of a compid (e.g. a.b.this.d, or a.b.this are both not allowed)
        if self.IDs[0] == "this":
            typeNode = self.env.getNode(self.typeName, "type")
            self.addToPrefix(typeNode)

            cNameNode = genNameNode(self.typeName)
            cNameNode.isThis = True
            self.prefixNodes.append(cNameNode)
            return True
        return False

    def checkLength(self):
        if not self.IDs:
            return True
        if self.IDs[0] == "length":
            return True
        return False

    # Checks and updates prefix if the next ID in self.IDs is a local variable or a parameter
    def checkLocalVar(self):
        if not self.IDs:
            return True
        if 'this' in self.name:
            return False
        ID = self.IDs[0]
        localVarNode = self.env.findNode(ID, "expr")
        if localVarNode:
            self.addToPrefix(localVarNode)

            cNameNode = genNameNode(self.typeName)
            cNameNode.nodeLink = self.env.getNode(ID, "expr")
            self.prefixNodes.append(cNameNode)
            return True

        return False

    # Checks and updates prefix if the next ID in self.IDs is in the contains set
    # TODO: figure out how to do the resolution after type checking is done
    def checkContains(self, update=False):
        if not self.IDs:
            return True

        ID = self.IDs[0]
        if self.env.findNode(ID, "fieldDcl") or self.env.findNode(ID, "method"):
            self.methodClass = self.env.getNode(self.typeName, 'type')
            self.prefixLink = "contain"
            self.addToPrefix("contain")

            # evaluate THIS first, fieldnode will be added to prefixNodes in checkType()
            if not self.prefixNodes:
                cNameNode = genNameNode(self.typeName)
                cNameNode.isThis = True
                self.prefixNodes.append(cNameNode)

            return True
        return False


    # Finding a static field
    def checkStatic(self):
        if not self.IDs:
            return True

        currPrefix = ""
        for index, ID in enumerate(self.IDs):
            if currPrefix:
                currPrefix += "."
            currPrefix += ID

            try:
                typeNode = self.env.getNode(currPrefix, "type")
            except Exception as e:
                continue

            # checking if the next ID is a static field in the class/interface
            if index+1 >= len(self.IDs):
                return False

            # if the next field is not a method inovocation
            # (it could only be a method invocation if self.methodinvoke is true and the next field is the last element in the compID)
            if not (self.methodInvoke and index+1 == len(self.IDs) - 1):
                staticFieldName = self.IDs[index+1]

                typeFieldNode = typeNode.env.getNode(staticFieldName, "fieldDcl")
                if "static" in typeFieldNode.mods:
                    self.prefixLink = typeFieldNode.variableDcl
                    self.staticField = typeFieldNode
                    # if it is primitive, then we leave it as a VarDclNode
                    if not self.prefixLink.dclType.myType.isPrimitive:
                        self.prefixLink = self.prefixLink.dclType.myType.typePointer

                    self.prefix = currPrefix + "." + staticFieldName
                    self.IDs = self.IDs[index+2:]

                    # if protected, check if we have access to it
                    if "protected" in typeFieldNode.mods:
                        checkProtected(typeFieldNode, self)

                    cNameNode = genNameNode(self.typeName)
                    cNameNode.nodeLink = typeFieldNode
                    cNameNode.isStatic = True
                    self.prefixNodes.append(cNameNode)

                    return True

                return False
            elif self.methodInvoke and index+1 == len(self.IDs) - 1:
                # TODO set the most recent? is this the correct implementation we want?
                self.methodClass = typeNode
                self.prefixLink = typeNode
                self.prefix = currPrefix
                self.IDs = self.IDs[index+1:]

                # If this is a static method, no codeGen needed at NameNode

                return True

        return False



    # Finds the shortest prefix that either is a local variable, instance field or a static field.
    # Raises error if no such prefix is found in the environment
    # ASSUMPTION: only the left most nodes in the AST
    #             (e.g. if there's sth like a.b.c().d, there would be 2 parse tree nodes: methodInvoke(a.b.c) and fieldAccess(d),
    #             then disambigName is only called on the methodInvoke node)
    def disambigName(self):
        # Checking if a1 is "this"
        if self.checkThis():
            self.pointToThis = True
            return

        # Checking if a1 is length
        if self.checkLength():
            return

        # Checking if a1 is a local variable
        if self.checkLocalVar():
            return

        # Checking if a1 is in contains set
        if self.checkContains():
            self.pointToThis = True
            return

        # Checking if the shortest prefix is a static field
        if self.checkStatic():
            self.shouldBeStatic = True
            return

        raise Exception("ERROR at disambiguating namespace: no prefix of name {} is found in environment.".format(self.name))

    def checkType(self):
        # type checking: go through each prefix and determine what type it is, get that type, and check if that type contains the next access
        # eg: a.b.c.d - disambigName would have already determined what the heck the shortest prefix is for this, so take that (let's say it's a.b) then check type c, see if it contains d, then get d return type and add it to self.myType

        if not self.prefixLink or self.prefixLink == 'contain':
            self.prefixLink = self

        curType = self.prefixLink

        if self.IDs:
            while self.IDs:
                if len(self.IDs) == 1:
                    if self.methodInvoke:
                        if curType.__class__.__name__ in ['ParamNode', 'VarDclNode', 'FieldNode']:
                            curType = curType.myType.typePointer
                            self.methodClass = curType
                        curType = curType.env.getNode(self.IDs[0], 'method')

                        if not hasattr(self, 'methodClass'):
                            meth = list(curType.values())[0]
                            self.methodClass = meth.env.getNode(meth.typeName, 'type')

                        self.methodName = self.IDs[0]

                    else:
                        if curType.myType and curType.myType.isArray and self.IDs[0] == 'length':
                            self.myType = TypeStruct("int", None)

                            cNameNode = genNameNode(self.typeName)
                            cNameNode.isLength = True
                            self.prefixNodes.append(cNameNode)
                            return
                        else:
                            if curType.__class__.__name__ in ['ParamNode', 'VarDclNode']:
                                curType = curType.myType.typePointer
                            curType = curType.env.getNode(self.IDs[0], 'fieldDcl')

                            cNameNode = genNameNode(self.typeName)
                            cNameNode.nodeLink = curType
                            self.prefixNodes.append(cNameNode)
                    # for methods, we want to keep prefixLink pointing to the class for getting method later
                else:
                    curType = curType.env.getNode(self.IDs[0], 'fieldDcl')

                    cNameNode = genNameNode(self.typeName)
                    cNameNode.nodeLink = curType
                    self.prefixNodes.append(cNameNode)

                # at this stage, all newly resolved field should be non static:
                if curType.__class__.__name__ == 'FieldNode':
                    if 'static' in curType.mods:
                        raise Exception("ERROR: Non-static access of static field {}".format(curType.name))

                    # if protected, check if we have access to it
                    if "protected" in curType.mods:
                        checkProtected(curType, self)

                self.prefix = self.prefix + "." + self.IDs[0]
                self.IDs.pop(0)

            self.prefixLink = curType

        # if self.prefixLink.__class__.__name__ == 'ParamNode':
        #     from pprint import pprint
        #     pprint(vars(self.prefixLink))
        #     self.prefixLink = self.prefixLink.myType.typePointer

        if self.methodInvoke:
            self.myType = 'TObeResolved'
        else:
            self.myType = self.prefixLink.myType

        if not self.myType:
            # from pprint import pprint
            # pprint(vars(self.prefixLink))
            # pprint(vars(self))
            raise Exception("ERROR: Cannot check type of name {}".format(self.name))

    # generates code that evaluates the address of whatever this NameNode is
    # result stored in eax (address)
    def addr(self):
        result = ""
        for c in self.prefixNodes[:-1]:
            result += c.codeGen()
        result += self.prefixNodes[-1].addr()
        return result


    def codeGen(self):
        self.code = ""
        for c in self.prefixNodes:
            self.code += c.codeGen()


# helper
def checkProtected(dcl, usage):
    # get curType's class it was declared in
    typeFieldNodeClass = dcl.env.getNode(dcl.typeName, "type")

    # get current class we're in
    curClass = usage.env.getNode(usage.typeName, "type")

    # check to see if curType's class is in the current class' super list
    if typeFieldNodeClass.canonName != curClass.canonName and typeFieldNodeClass.canonName not in getSupers(curClass) and typeFieldNodeClass.packageName != curClass.packageName:
        raise Exception("ERROR: Class {} is attempting to access a protected field from class {}".format(curClass.canonName, typeFieldNodeClass.canonName))
