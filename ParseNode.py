import string
import sys

##################### Nodes ##########################################

# children: Node[]
# name:     string
# lex:      string
class Node():
    def __init__(self, name, lex, children):
        self.name     = name
        self.lex      = lex
        self.children = children
    def __str__(self):
        return printNodePretty(self)
    def weed(self):
        return ''

##################### Helpers ##########################################

# node:   Node
# prefix: string
# last:   boolean
def printNodePretty(node, prefix='', last=True):
    res = prefix
    res += '`- ' if last else '|- '
    res += node.name + ' ' + node.lex + '\n'
    prefix += '   ' if last else '|  '
    num = len(node.children)
    for i, child in enumerate(node.children):
        last = i == (num - 1)
        res += printNodePretty(child, prefix, last)
    return res
