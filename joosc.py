import sys
import random as random
from os import listdir
from os.path import isfile, join

from Scanning import scan
from Parsing import parse
from AstBuilding import astBuild, buildEnvAndLink, disamiguateAndTypeChecking, reachabilityChecking, codeGen
import Weeding


def main():
    inputFiles = [f for f in sys.argv[1:]]

    parseTrees = []

    for f in inputFiles:
        content = open(f, "r").read()

        # Scanning
        try:
            (tokens, error) = scan(content)
        except:
            return 42
        if tokens is None:
            return 42

        # Weeding after scanning
        # No weeds if everything is good (weeds = None)
        weeds = Weeding.fileNameCheck(tokens, f)
        if weeds:
            return 42

        # Parsing
        tree = None
        try:
            (tree, error) = parse(tokens)
        except:
            return 42

        # Error in Parsing
        if tree is None:
            return 42

        parseTrees.append((f, tree))

    # Building ASTs from all parse trees
    try:
        ASTs = astBuild(parseTrees)
    except Exception as e: # for definite assignemnt
        return 42
    
    try:
        buildEnvAndLink(ASTs)
    except Exception as e:
        return 42 # double local variable declarations

    try:
        disamiguateAndTypeChecking(ASTs)
    except Exception as e:
        return 42

    try:
        reachabilityChecking(ASTs)
    except Exception as e:
        return 42

    try:
        codeGen(ASTs)
    except Exception as e:
        return 42

    return 0



re = main()
print(re)
