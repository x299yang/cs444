#!/bin/bash

# can supply folder name as argument to run 1 test test case
# run one case: ./testAssemble.sh J1_00_Step0.java
# run all cases: ./testAssemble.sh

cd output

if [ "$#" -eq 0 ]; then
  # assemble and link and output results of running each test case
  for d in ./*/; do
    cd $d
    echo "-----------------------------------"
    echo "Run generated code for " + $d

    for filename in ./*.s; do
      /u/cs444/bin/nasm -O1 -f elf -g -F dwarf $filename
    done
    ld -melf_i386 -o main ./*.o

    # run main
    ./main

    # echo the exit code
    echo $?
    echo "-----------------------------------"

    cd ..
  done
else
  cd $1
  for filename in ./*.s; do
    /u/cs444/bin/nasm -O1 -f elf -g -F dwarf $filename
  done
  ld -melf_i386 -o main ./*.o

  # run main
  ./main

  # echo the exit code
  echo $?
fi
